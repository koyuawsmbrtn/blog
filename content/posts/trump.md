+++
title = "The thing with Trump's Twitter account"
date = "2021-01-09"
author = "koyu"
cover = "https://images.unsplash.com/photo-1585245802021-88032c9350c6?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&h=452&q=80"
showFullContent = true
+++

Trump has been banned from Twitter today, as many are celebrating this event I want to give some insights on why this happened and give some comments on this.

## Terms of Service

By signing up for any internet service you are agreeing to the Terms of Service of that specific platform. You can kind of think of like the house rules in a bar everyone has to follow. If the barkeeper doesn't like you and thinks you violated their house rules they'll kick you out. It's not like this has been a thing that's new to the internet in general, the whole concept existed ages ago. If you violate the [Terms of Service for Twitter](https://help.twitter.com/en/rules-and-policies/twitter-rules) they'll kick you out, if you violate the [content guidelines of koyu.space](https://koyu.space/about/more) we'll kick you out.

## Twitter

Twitter is unique in its organization structure. They are a for-profit company that has a lot of different people working on it with a centralized approach that keeps their users locked into the platform while making money off user data to meet their financial goals. This sounds very long and probably very weird to you, but it's basically what Twitter is. The problem with that approach is that the company can't moderate agile which leads to some content being left out moderated or moderated "too much". This is a problem especially if [real life events materialize outside of Twitter](https://youtu.be/aHmTzFnymOM).

## Mastodon

<iframe src="https://mastodon.social/@Gargron/105522923930597280/embed" border="0" style="border:0;" width="640" height="420"></iframe>

Eugen (the guy behind Mastodon) wrote a [blog post](https://blog.joinmastodon.org/2018/03/twitter-is-not-a-public-utility/) back in 2018 explaining why Twitter is the way it is and I can't agree more: Twitter is **not** a public utility and that it takes forever for Twitter to take action. I also think that federation will solve most of the problems and criticism that Twitter is currently facing while everyone can be happy. Not a large US-based corperation decides when content is getting moderated, the admins of each server do, which is great.