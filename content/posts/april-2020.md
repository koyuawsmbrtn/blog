+++
title = "The months on koyu.space (March-April 2020)"
date = "2020-04-14"
author = "koyu"
cover = "https://images.unsplash.com/photo-1434030216411-0b793f4b4173?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
showFullContent = true
+++

The development work is now over for April 2020 and we are ready to present everything that happened around koyu.space to you. A lot of things happened and we don't want you to miss out on all these important changes.

# The big summer migration

You might have heard that we are going to migrate to a new host on June 26 2020 (note that down in your calendar). This means we would then have a lot more flexibility than we already have which also means that it will allow us to introduce new features. We are also currently thinking about migrating to glitch-soc which should go flawlessly. If the migration was successful all users will get an e-mail (and our people on Zulip and the app will get a notification as well) so make sure you check your inbox on a regular basis on this day. This is a huge step and we can't wait to introduce you to the new features glitch-soc will offer you. Note that we will discontinue the Matrix server so make sure that you migrate your Matrix account to another instance (like ponies.im or yuri.im, matrix.org is too huge to handle the traffic fast enough) and we will also discontinue the Git server so make sure you get your code off it before it's being shut down. We recommend using many of the free code hosting sites like Gitlab or GitHub.

# The koyu.space app now has notifications

What's making this sound? Brrring! It's the new koyu.space app with notifications. Everyone who downloaded the APK file or the app from Google Play will receive notifications about important announcements, updates and things that happened inside of koyu.space right on their phones. The F-Droid version won't receive updates anymore, but since there has been no security flaws or new features apart from notifications (which requires the use of proprietary Google libraries) it's not a big deal. The push backend is completely [open-source](https://gitlab.com/koyuawsmbrtn/koyuspace-app-pushservice) like everything we do, so take a look at the code and report anything unusual to us.

![koyu.space App - Push Notifications on lock screen](/uploads/app-lockscreen-push.png)

# A more professional style

We dropped our old bunny-pal from the loading animations and replaced it with a soothing dot animation. Even though we dropped the hopping bunny animation from the loading screen the bunny will still cry if something bad happened. We also changed the background on the front page to match it more with our server banner and cleaned up the front page to only display the most useful information to people. The change should display koyu.space in a more professional manner, but also keep the overall style that makes koyu.space stand out as well. The koyu.space desktop app also received these changes.

![koyu.space Desktop - Loading](/uploads/desktop.png)

# Minor updates

The koyu.space base (Mastodon) has been updated to version 3.1.3 and the Matrix Synapse server has been updated to version 1.12.3. The Matrix server will receive updates until this summer until it's being shut down at the big summer migration. koyu.space Git has been updated to Gitea version 1.11.2 and will also receive updates until the big summer migration. We also fixed the issue on koyu.space Git which has lead to some browsers incorrectly retrieve the custom stylesheet.

# Offtopic

koyu also recently discovered Overwatch for herself and invites everyone to play a round or two. Hit her up on Battle.net at bubblineyuri#2146 for some games with the community. It will be a lot of fun.

<video src="https://f001.backblazeb2.com/file/koyuspace-media/media_attachments/files/003/411/609/original/8296cc4053594253.mp4" poster="https://f001.backblazeb2.com/file/koyuspace-media/media_attachments/files/003/411/609/small/8296cc4053594253.png" preload="metadata" role="button" tabindex="0" width="610" height="343.125" volume="1" controls></video>