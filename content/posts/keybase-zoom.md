+++
title = "koyu.space and what the Keybase/Zoom acquisition means"
date = "2020-05-07"
author = "koyu"
cover = "/uploads/zoom.png"
showFullContent = true
+++

Keybase has been acquired by Zoom. This is huge news for us, but it also means that a company known to create secure software is now in the hands of a company that had done years of damage to privacy and user trust. Today we are not going to support Keybase in the future and cease any integration of Keybase into koyu.space services. This also means you have to download new builds for the koyu.space desktop app any time soon as files were partially hosted on Keybase. Existing builds continue to run, but will fail updating.

Have a nice day and stay safe 💪