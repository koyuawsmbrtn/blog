+++
title = "The months on koyu.space (September & October 2020)"
date = "2020-11-05"
author = "koyu"
cover = "https://images.unsplash.com/photo-1518432031352-d6fc5c10da5a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80"
showFullContent = true
+++

The development work is now over for September and October 2020 and we are ready to present everything that happened around koyu.space to you. A lot of things happened and we don’t want you to miss out on all these important changes.

# Fix image uploads

We finally fixed broken images showing up everywhere on koyu.space and we hope you'll like [this change](https://github.com/koyuspace/mastodon/commit/62fcf03e62ef46594e62ae776daeaa88c010207f).

# Post notifications

Wanted to know when the least active person in your feed posted something after a while? Now you can since you can now be notified for every post this person makes. Simply click the bell icon in the user profile and you'll be notified. Right now there's no API for that notification type so you won't receive push notifications from the koyu.space app regarding these notifications.

# Bunny ears

Everyone now has bunny ears around their avatar. We think that's fun. Just check it out.

# New theme: Pumpkin

We got a new theme, the "Pumpkin" theme for Halloween (which is over now). This orange/black combo looks really nice.

# Picture-in-Picture mode for desktop

Now you can watch videos or listen to music while you scroll through the timeline at once! Welcome to the 21st century.

# We moved from Liberapay to Creapaid

Creapaid is a payment platform for recurring donations made by [stux](https://mstdn.social/@stux) which takes way less fees than Liberapay, looks nicer and also allows us to better find out who donated so we can give away the perks you'd get if you donate. If you want to donate once you can still do that on the [koyu.space Shop](https://shop.koyu.space/product/sponsorship/).

# Announcing koyu.space Chat

[koyu.space Chat](https://chat.koyu.space) is now our preffered chat platform. It's being bridged to our [Telegram group](https://t.me/koyuspace), IRC and [Discord server](https://discord.gg/8gbd7YR).

# Smaller changes

We also had a few smaller changes:

- Re-enabled back button in user settings
- Fix a hell lot of awful crashes
- Chromium scrollbars should look a lot nicer now
- Redesigned login page
- Fixes for the "unread notifications" marker
- More German
- More bunnies
- and more fun!

# Offtopic

Nothing here. Check back later.